<h1>Employment</h1>
<hr/>
A simple app that enables employers to post job offers
and browse candidates, and candidates expose themselves to recruiters and browse job offers
<hr/>
This is the server side / backend portion of the app. 
<hr/>
<h3>Technologies:</h3>
<ul>
    <li>Java Spring Boot</li>
    <li>MariaDB</li>
    <li>JWT</li>
</ul>