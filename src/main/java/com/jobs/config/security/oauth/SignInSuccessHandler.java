package com.jobs.config.security.oauth;

import com.jobs.config.security.JwtHelper;
import com.jobs.config.security.JwtHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;

public class SignInSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private final JwtHelper jwtHelper;

    @Value("${app.client_url}")
    public String clientAppUrl;

    private String provider;

    public SignInSuccessHandler(String provider, JwtHelper jwtHelper) {
        this.provider = provider;
        this.jwtHelper = jwtHelper;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws ServletException, IOException {
        try {
            String jwtToken = obtainToken(authentication);

            response.sendRedirect(this.clientAppUrl + "/login.html?status=oauth-success&token=" + jwtToken);

        } catch (Exception exception) {
            logger.error("Login problem: ", exception);
            exception.printStackTrace();
            response.sendRedirect(this.clientAppUrl + "/login.html?status=oauth-failure");
        }

        super.onAuthenticationSuccess(request, response, authentication);
    }


    public String obtainToken(Authentication authentication) {
        OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) authentication;
        LinkedHashMap authDetailsMap = (LinkedHashMap) oAuth2Authentication.getUserAuthentication().getDetails();


        String providerId = authDetailsMap.containsKey("id")
                ? authDetailsMap.get("id").toString()
                : authDetailsMap.get("sub").toString();

        return this.jwtHelper.generateToken(providerId, provider, authDetailsMap);
    }

}

