package com.jobs.config.security.oauth;

import com.jobs.config.security.JwtHelper;
import com.jobs.config.security.JwtHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@OAuthProvider
public class GithubAuthentication extends AbstractOAuthConfigurer {

    @Autowired
    public GithubAuthentication(@Qualifier("oauth2ClientContext") OAuth2ClientContext oauth2ClientContext,
                                JwtHelper jwtHelper) {
        super(oauth2ClientContext, jwtHelper);
    }

    @Bean(name = "githubSuccessHandler")
    @Override
    protected AuthenticationSuccessHandler successHandler() {
        return new SignInSuccessHandler("GITHUB", this.jwtHelper);
    }

    @Bean(name = "githubFailureHandler")
    @Override
    protected AuthenticationFailureHandler failureHandler() {
        return new SignInFailureHandler();
    }

    @Bean(name = "githubConfig")
    @ConfigurationProperties("github")
    public ClientResources clientConfig() {
        return new ClientResources();
    }

    @Override
    protected String entryPointUrl() {
        return "/login/github";
    }
}

