package com.jobs.config.security;

import com.jobs.service.UserService;
import com.jobs.service.UserService;
import io.jsonwebtoken.Claims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtHelper tokenProvider;

    @Autowired
    private UserService userAccessService;

    private final JwtHelper jwtHelper;


    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    public JwtAuthenticationFilter(JwtHelper jwtHelper) {
        this.jwtHelper = jwtHelper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String jwt = getJwtFromRequest(request);
            if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
                String provider = tokenProvider.getClaims(jwt).get("provider", String.class);
                if(provider.equals("SYSTEM")) {
                    Long userId = tokenProvider.getUserIdFromJWT(jwt);
                    UserDetails userDetails = userAccessService.findUserById(userId);
                    userDetails.getAuthorities();
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                } else {
                    Claims claims = jwtHelper.getClaims(jwt);
                    List grantedAuthorities = Stream.of(new SimpleGrantedAuthority("ROLE_USER"))
                            .collect(Collectors.toList());
                    String picture = "";
                    String email = claims.get("email", String.class);
                    if(claims.get("provider", String.class).equals("FACEBOOK")) {
                        picture = "http://graph.facebook.com/"+claims.get("id", String.class)+"/picture?type=large";
                    } else if(claims.get("provider", String.class).equals("GOOGLE")) {
                        picture = claims.get("picture", String.class);
                    } else if(claims.get("provider", String.class).equals("GITHUB")) {
                        picture = claims.get("avatar_url", String.class);
                        email = claims.get("blog", String.class);
                    }

                    UserPrincipal principal = new UserPrincipal(0L, claims.get("name", String.class), email, "", claims.get("provider", String.class), picture, grantedAuthorities);
                    SecurityContextHolder
                            .getContext()
                            .setAuthentication(new PreAuthenticatedAuthenticationToken(principal, null, grantedAuthorities));
                }
            }
        } catch (Exception ex) {
            logger.error("Could not set user authentication in security context", ex);
        }

        filterChain.doFilter(request, response);
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }
}