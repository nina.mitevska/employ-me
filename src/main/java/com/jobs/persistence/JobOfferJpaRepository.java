package com.jobs.persistence;

import com.jobs.model.JobOffer;
import com.jobs.model.Skill;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface JobOfferJpaRepository extends
        JpaRepository<JobOffer, Long>
//        PagingAndSortingRepository<JobOffer, Long>,
//        QueryByExampleExecutor<JobOffer>
{
    List<JobOffer> findByEmployerId(Long employerId);

    @Query(value = "select * from job_offer j join job_required_skills jrs on j.id=jrs.job_offer_id " +
            "where jrs.required_skill_id=:query_skill_id", nativeQuery = true)
    List<JobOffer> findAllByRequiredSkill(@Param("query_skill_id") Long skillId);
/*
"SELECT e FROM Professor e
WHERE NOT EXISTS (SELECT p FROM e.phones p)"
*/

    // Functional!
    /**
     *
     * @param skills the skill ids which are relevant for the search
     * @param skillsNum the number of the skills, needed in the sql query
     * @return all job offers that have EVERY skill in "skills" as a required skill
     */
    @Query(value = "select * from job_offer j  " +
            "where :skills_num =  " +
            "   ( select count(*) from job_required_skills jrs " +
            "   where j.id=jrs.job_offer_id and jrs.required_skill_id in :req_skills )", nativeQuery = true)
    List<JobOffer> customFindAllByRequiredSkills(@Param("req_skills") List<Long> skills, @Param("skills_num") int skillsNum);

    @Query(value = "select * from job_offer j  " +
            "where :skills_num =  " +
            "   ( select count(*) from job_required_skills jrs " +
            "   where j.id=jrs.job_offer_id and jrs.required_skill_id in :req_skills )", nativeQuery = true)
    Page<JobOffer> customFindAllByRequiredSkillsPage(@Param("req_skills") List<Long> skills, @Param("skills_num") int skillsNum, Pageable pageable);

    /*
            Long id,
            String name,
            String location,
            String employerName,
            List<String> requiredSkills,
            String shortJobDescription
     */

//    @Query(value = "select  * " +
//            "from job_offer j  " +
//            "where :skills_num =  " +
//            "   ( select count(*) from job_required_skills jrs " +
//            "   where j.id=jrs.job_offer_id and jrs.required_skill_id in :req_skills )", nativeQuery = true)
//    Page<JobSearchResult> customFindAllByRequiredSkillsPageJSR(@Param("req_skills") List<Long> skills, @Param("skills_num") int skillsNum, Pageable pageable);

//    @Query(value = "select * from job_offer j  " +
//            "where :skills_num =  " +
//            "       ( select count(*) from :jo_search jrs " +
//            "       where j.id=jrs.job_offer_id and jrs.required_skill_id in :req_skills )" +
//            "   and ", nativeQuery = true)
    @Query(value = "select * from job_offer j " +
            "where j.name like CONCAT('%', :jo_search.name, '%')", nativeQuery = true)
    Page<JobOffer> customFindAllByMultipleSeparateFields(@Param("jo_search") JobOffer jobOfferSearch, Pageable pageable);

    List<JobOffer> findAllByNameContaining(String name);
    List<JobOffer> findAllByShortJobDescriptionContaining(String desc);
    List<JobOffer> findAllByLocationContaining(String location);

    @Query(value = "select * from job_offer j join app_user emp on j.emp_id = emp.id  " +
            "where j.name in :properties" +     // raboti samo ako se napise identicno
            "   or j.location in :properties" + // raboti samo ako se napise identicno
            "   or emp.name in :properties" +   // raboti samo ako se napise identicno
            "   or j.shortJobDescription in :properties" + // nema logika, treba da se bara del od zbor VO shortJobDesc
            "   or j.required_skill_id", nativeQuery = true)
    Page<JobOffer> customFindAllByMultipleMixedFields(@Param("properties") String[] properties, Pageable pageable);

    //@EntityGraph(value = "full-job-offer-graph")
    // A native SQL query cannot use EntityGraphs
    @Query(value = "select j.id, s.id, i.id, j.emp_id, j.location, j.name, j.short_desc, emp.name, s.name, i.name " +
            "from (job_offer j join app_user emp on j.emp_id = emp.id) " +
            "   join job_required_skills jrs on j.id = jrs.job_offer_id join skill s on s.id = jrs.required_skill_id " +
            "   join industry i on s.industry_id = i.id " +
            "where j.name like CONCAT('%',:property,'%')" +     //
            "   or j.location like CONCAT('%',:property,'%')" + //
            "   or emp.name like CONCAT('%',:property,'%')" +   //
            "   or j.short_desc like CONCAT('%',:property,'%')" + //
            "   or s.name like CONCAT('%',:property,'%')" +
            "   or i.name like CONCAT('%',:property,'%')" +
            "", nativeQuery = true)
    List<JobOffer> customFindAllBySingleUnknownField(@Param("property") String property);

//    @Query(value = "select j.id, s.id, i.id, j.emp_id, j.location, j.name, j.short_desc, emp.name, s.name, i.name " +
//            "from (job_offer j join app_user emp on j.emp_id = emp.id) " +
//            "   join job_required_skills jrs on j.id = jrs.job_offer_id join skill s on s.id = jrs.required_skill_id " +
//            "   join industry i on s.industry_id = i.id " +
//            "where j.name like CONCAT('%',:property,'%')" +     //
//            "   or j.location like CONCAT('%',:property,'%')" + //
//            "   or emp.name like CONCAT('%',:property,'%')" +   //
//            "   or j.short_desc like CONCAT('%',:property,'%')" + //
//            "   or s.name like CONCAT('%',:property,'%')" +
//            "   or i.name like CONCAT('%',:property,'%')" +
//            "", nativeQuery = true)
//    List<JobOfferView> JSRCustomFindAllBySingleUnknownField(@Param("property") String property);

    Page<JobOffer> findByRequiredSkills(Skill skill, Pageable pageable);

    Page<JobOffer> findAllByRequiredSkills(List<Skill> requiredSkills, Pageable pageable);

    Page<JobOffer> findAllBy(Example<JobOffer> jobOfferExample, Pageable pageable);


    //Page<JobOffer> findAllByEmployerOrLocationOrShortJobDescriptionContainsOrName();

    //List<JobOffer> findAllByRequiredSkillsContaining(List<Skill> skills);
/*
    List<JobOffer> findAllByRequiredSkills(List<Skill> skills);

 */
}
