package com.jobs.persistence;

import com.jobs.model.Industry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by HTomovski
 * Date: 09-Sep-19
 * Time: 1:47 PM
 */
@Repository
public interface IndustryJpaRepository extends JpaRepository<Industry, Long> {
}
