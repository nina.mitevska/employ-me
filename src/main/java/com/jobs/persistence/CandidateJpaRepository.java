package com.jobs.persistence;

import com.jobs.model.Candidate;
import com.jobs.model.Skill;
import com.jobs.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateJpaRepository extends JpaRepository<Candidate, Long> {

    Page<Candidate> findBySkills (Skill skill, Pageable pageable);
}
