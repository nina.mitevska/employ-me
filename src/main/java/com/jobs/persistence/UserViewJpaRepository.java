package com.jobs.persistence;

import com.jobs.model.User;
import com.jobs.model.views.UserViewShort;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserViewJpaRepository extends JpaRepository<UserViewShort, Long> {
    //UserViewShort findById(Long id);
    UserViewShort findByNameContaining(String name);
}
