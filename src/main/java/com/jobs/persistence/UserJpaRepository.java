package com.jobs.persistence;

import com.jobs.model.Skill;
import com.jobs.model.User;
import com.jobs.model.Skill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by HTomovski
 * Date: 22-Aug-19
 * Time: 3:36 PM
 */
public interface UserJpaRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);

    //List<User> findAll();

    //Page<User> findBySkills (Skill skill, Pageable pageable);

    List<User> findAllByNameContaining(String name);

}
