package com.jobs.persistence;

import com.jobs.model.Skill;
import com.jobs.model.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SkillJpaRepository extends JpaRepository<Skill, Long> {
    Optional<Skill> findByName(String skillName);

    List<Skill> findAllByIndustry_Id(Long industryId);
}
