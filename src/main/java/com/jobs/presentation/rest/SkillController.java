package com.jobs.presentation.rest;

import com.jobs.model.Skill;
import com.jobs.model.payloads.requests.CreateSkillRequest;
import com.jobs.service.SkillService;
import com.jobs.model.Skill;
import com.jobs.service.SkillService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by HTomovski
 * Date: 08-Sep-19
 * Time: 9:58 AM
 */
@RestController
@CrossOrigin({"*", "localhost:3000"})
@RequestMapping("/skills")
public class SkillController {
    private final SkillService skillService;

    public SkillController(SkillService skillService) {
        this.skillService = skillService;
    }

    @GetMapping("/{id}")
    public List<Skill> getSkillsByIndustry(@PathVariable(name = "id") Long industryId){
        return skillService.getSkillsByIndustryId(industryId);
    }
//    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
//    @PutMapping("/{skill-name}/{industry-id}")
//    public Skill createSkill(@PathVariable(name = "skill-name") String newSkillName,
//                             @PathVariable(name = "industry-id") Long industryId){
//        return skillService.createSkill(newSkillName, industryId);
//    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @PutMapping
    public Skill createSkill(@NotNull @RequestBody CreateSkillRequest createSkillRequest){
        return skillService.createSkill(createSkillRequest.skillName, createSkillRequest.industryId);
    }
}
