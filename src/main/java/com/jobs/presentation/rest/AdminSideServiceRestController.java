package com.jobs.presentation.rest;

import com.jobs.model.*;
import com.jobs.service.AdminSideService;
import com.jobs.service.AdminSideService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by HTomovski
 * Date: 15-Jul-19
 * Time: 7:17 PM
 */

@RestController
@CrossOrigin({"*", "localhost:3000"})
@RequestMapping(value = "/admin")
public class AdminSideServiceRestController {
    private final AdminSideService service;

    public AdminSideServiceRestController(AdminSideService service) {
        this.service = service;
    }

    @PutMapping("/skill/create")
    public Skill createNewSkill(@RequestParam(value = "skillName") String skillName,
                                @RequestParam(value = "industryId") Industry industry) {
        return service.createNewSkill(skillName, industry);
    }
    @PatchMapping("/skill/update")
    public Skill updateSkill(@RequestParam Long skillId, @RequestParam String skillName){
        return service.updateSkill(skillId,skillName);
    }
    @GetMapping("/skill/viewSkillById/{skillId}")
    public Optional<Skill> viewSkillById(@PathVariable(value = "skillId") Long skillId){
        return service.viewSkill(skillId);
    }

    @GetMapping("/skill/viewSkillByName/{skillName}")
    public Optional<Skill> viewSkillByName(@PathVariable(value = "skillName") String skillName){
        return service.viewSkill(skillName);
    }

    @DeleteMapping("/skill/delete")
    public void removeSkill(@RequestParam Long skillId){
        service.removeSkill(skillId);
    }

    @PutMapping("/client/create")
    public User createNewUser(@RequestBody User user){
        return service.createNewUser(user);
    }

    @PatchMapping("/client/update")
    public User updateUser(@RequestBody User user){
        return service.updateUser(user);
    }

    @GetMapping("/client/viewClientById/{userId}")
    public User viewClientById(@PathVariable(value = "userId") Long userId){
        Optional<User> clientOpt = service.viewUser(userId);
        User client = null;
        if(clientOpt.isPresent()){
            client = clientOpt.get();
        }
        return client;
    }

    @DeleteMapping("client/delete/{userId}")
    public void removeClient(@PathVariable(value = "userId") Long userId){
        service.removeUser(userId);
    }


    @PutMapping("/job-offer/create")
    public JobOffer createNewJobOffer(@RequestBody JobOffer jobOffer){
        return service.createNewJobOffer(jobOffer);
    }
    @PatchMapping("/job-offer/update")
    public JobOffer updateJobOffer(@RequestBody JobOffer jobOffer){
        return service.updateJobOffer(jobOffer);
    }
    @GetMapping("/job-offer/view-by-id/{jobOfferId}")
    public JobOffer viewJobOfferById(@PathVariable(value = "jobOfferId") Long jobOfferId){
        Optional<JobOffer> jobOfferOpt = service.viewJobOffer(jobOfferId);
        JobOffer jobOffer = null;
        if(jobOfferOpt.isPresent()){
            jobOffer = jobOfferOpt.get();
        }
        return jobOffer;
    }
    @DeleteMapping("job-offer/delete/{jobOfferId}")
    public void removeJobOffer(@PathVariable(value = "jobOfferId") Long jobOfferId){
        service.removeJobOffer(jobOfferId);
    }


}
