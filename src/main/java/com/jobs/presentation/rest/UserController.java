package com.jobs.presentation.rest;

import com.jobs.config.security.CurrentUser;
import com.jobs.config.security.JwtHelper;
import com.jobs.config.security.UserPrincipal;
import com.jobs.model.Candidate;
import com.jobs.model.Employer;
import com.jobs.model.exceptions.NoSuchUserException;
import com.jobs.model.User;
import com.jobs.model.payloads.requests.LoginRequest;
import com.jobs.model.payloads.requests.RegisterRequest;
import com.jobs.model.payloads.responses.ApiResponse;
import com.jobs.model.payloads.responses.JwtAuthenticationResponse;
import com.jobs.service.UserService;
import com.jobs.config.security.CurrentUser;
import com.jobs.config.security.JwtHelper;
import com.jobs.config.security.UserPrincipal;
import com.jobs.model.User;
import com.jobs.model.exceptions.NoSuchUserException;
import com.jobs.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;
import java.util.Optional;

/**
 * Created by HTomovski
 * Date: 08-Sep-19
 * Time: 9:58 AM
 */
@RestController
@CrossOrigin({"*", "localhost:3000"})
@RequestMapping("/users")
public class UserController {

    @Value("${app.jwtExpirationInMs}")
    private int JWT_EXPIRATION_MS;

    private final UserService userService;

    private final AuthenticationManager authenticationManager;

    private final JwtHelper tokenProvider;

    public UserController(UserService userService, AuthenticationManager authenticationManager, JwtHelper tokenProvider) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
    }

    // LOGIN
    @PostMapping(value = "/login")
    public ResponseEntity<?> loginUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, JWT_EXPIRATION_MS));
    }

    //REGISTER
    @PostMapping(value = "/register/candidate")
    public ResponseEntity<?> newCandidate(@NotNull @RequestBody RegisterRequest registerRequest) {
        Candidate candidate = userService.saveCandidate(registerRequest.getName(), registerRequest.getEmail(), registerRequest.getPassword());
        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users").buildAndExpand().toUri();
        return ResponseEntity.created(location).body(new ApiResponse(true, candidate.id.toString()));
    }
    @PostMapping(value = "/register/employer")
    public ResponseEntity<?> newEmployer(@NotNull @RequestBody RegisterRequest registerRequest) {
        Employer employer = userService.saveEmployer(registerRequest.getName(), registerRequest.getEmail(), registerRequest.getPassword());
        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users").buildAndExpand().toUri();
        return ResponseEntity.created(location).body(new ApiResponse(true, employer.id.toString()));
    }

    //GET USER BY ID
    @GetMapping("/{userId}")
    public User getUserById(@PathVariable(name = "userId") Long userId) {
        Optional<Candidate> candidateOptional = userService.findCandidateById(userId);
        if (candidateOptional.isPresent())
            return candidateOptional.get();
        Optional<Employer> employerOptional = userService.findEmployerById(userId);
        if (employerOptional.isPresent())
            return employerOptional.get();
        Optional<User> user = userService.findById(userId);
        return user.orElseThrow(NoSuchUserException::new);
    }

    // go vrakja userot duri i ako e tokenot expired
    @PreAuthorize("hasAnyRole('ROLE_USER, ROLE_ADMIN')")
    @GetMapping("/me")
    public UserPrincipal getCurrentUserPrincipal(@CurrentUser UserPrincipal user) {
//        Optional<Candidate> candidateOptional = userService.findCandidateById(user.getId());
//        if (candidateOptional.isPresent())
//            return candidateOptional.get();
//        Optional<Employer> employerOptional = userService.findEmployerById(user.getId());
//        if (employerOptional.isPresent())
//            return employerOptional.get();
//        return userService.findById(user.getId()).orElseThrow(NoSuchUserException::new);
        return user;
    }

    @GetMapping("/skill/{skillId}/{pageNum}")
    public Page<Candidate> getCandidatesBySkill(@PathVariable("skillId") Long skillId,
                                      @PathVariable("pageNum") int pageNum) {
        return userService.findAllBySkill(skillId, pageNum);
    }

    @GetMapping("/skills/{skillIds}")
    public List<Candidate> getCandidatesBySkills(@NotNull @PathVariable("skillIds") List<Long> skillIds) {
        return userService.findAllBySkills(skillIds);
    }

    @PatchMapping()
    public User updateUser(@RequestBody User user){
        return userService.save(user);
    }

    @PatchMapping("/candidate")
    public Candidate updateCandidate(@RequestBody Candidate candidate){
        return userService.updateCandidate(candidate);
    }

    @PatchMapping("/employer")
    public Employer updateEmployer(@RequestBody Employer employer){
        return userService.updateEmployer(employer);
    }


}
