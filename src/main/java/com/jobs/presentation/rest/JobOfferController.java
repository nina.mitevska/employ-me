package com.jobs.presentation.rest;

import com.jobs.config.security.CurrentUser;
import com.jobs.config.security.UserPrincipal;
import com.jobs.model.exceptions.NoSuchJobOfferException;
import com.jobs.model.JobOffer;
import com.jobs.model.views.JobOfferView;
import com.jobs.service.JobOfferService;
import com.jobs.service.UserService;
import com.jobs.config.security.CurrentUser;
import com.jobs.config.security.UserPrincipal;
import com.jobs.model.exceptions.NoSuchJobOfferException;
import com.jobs.service.JobOfferService;
import com.jobs.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by HTomovski
 * Date: 19-Oct-19
 * Time: 7:46 PM
 */
@RestController
@CrossOrigin({"*", "localhost:3000"})
@RequestMapping("/job-offers")
public class JobOfferController {
    private final UserService userService;
    private final JobOfferService jobOfferService;

    public JobOfferController(UserService service, JobOfferService jobOfferService) {
        this.userService = service;
        this.jobOfferService = jobOfferService;
    }

    // === Create ===
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public JobOffer createJobOffer(@RequestBody JobOffer jobOffer,
                                   @CurrentUser UserPrincipal userPrincipal) {
        return jobOfferService.createJobOffer(jobOffer, userPrincipal);
    }

    /*
        @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
        @PutMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
        public JobOffer createJobOfferSeparateParams(@RequestParam String name,
                                                     @RequestParam String location,
                                                     @RequestParam Long employerId,
                                                     @RequestParam List requiredSkills,
                                                     @RequestParam String description) {
            JobOffer jobOffer = new JobOffer();
            jobOffer.name = name;
            jobOffer.location = location;
            userService.findById(employerId).ifPresent(user -> jobOffer.employer = user);

            return jobOfferService.createJobOffer(jobOffer);
        }
    */
//    @PostMapping("/example/{page}")
//    public Page<JobOffer> viewByExample(@RequestBody JobOffer exampleJobOffer,
//                                        @PathVariable(name = "page") int page) {
//        return jobOfferService.browseJobOffersByExample(exampleJobOffer, page);
//    }
    //        String name, String location, User employer, List<Skill> requiredSkills,
    //        String shortJobDescription
//    @PostMapping("/example/{page}")
//    public Page<JobOffer> viewByExample(
//            @RequestBody String name,
//            @RequestBody String location,
//            @RequestBody String location,
//            @PathVariable(name = "page") int page) {
//        JobOffer exampleJobOffer = new JobOffer();
//        return jobOfferService.browseJobOffersByExample(exampleJobOffer, page);
//    }

    // === Edit ===
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @PatchMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public JobOffer editJobOffer(@RequestBody JobOffer jobOffer,
                                 @CurrentUser UserPrincipal userPrincipal) {
        return jobOfferService.editJobOffer(jobOffer, userPrincipal);
    }

    // === Delete ===
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @DeleteMapping("/{id}")
    public void closeJobOffer(@PathVariable("id") Long jobOfferId,
                              @CurrentUser UserPrincipal userPrincipal) {
        System.out.printf("Deletion for job %d started.\n", jobOfferId);
        System.out.printf("Current user id is %d.\n", userPrincipal.getId());
        jobOfferService.closeJobOffer(jobOfferId, userPrincipal);
    }

    // === Search ===

    @GetMapping(value = "/simple-search")
    public Page<JobOfferView> browseByMultipleMixedFields(@RequestParam("searchParams") List<String> searchParams,
                                                          @RequestParam("page") int page) {
        System.out.println(searchParams);

        return jobOfferService.browseJobOffersByMultipleMixedFields(searchParams, page);
    }

//    @PostMapping(value = "/simple-search", consumes = MediaType.APPLICATION_JSON_VALUE)
//    public Page<JobOfferView> browseByMultipleMixedFieldsPost(@RequestBody String[] responseBody) {
//        return jobOfferService.browseJobOffersByMultipleMixedFields(responseBody[0], Integer.parseInt(responseBody[1]));
//    }
//    @GetMapping("/simple-search/{params}/{page}")
//    public Page<JobOfferView> browseByMultipleMixedFields(@PathVariable(name = "params") String propertyList,
//                                                          @PathVariable(name = "page") int page) {
//        return jobOfferService.browseJobOffersByMultipleMixedFields(propertyList, page);
//    }
//


    @GetMapping("/advanced")
    public Page<JobOfferView> browseByMultipleSeparateFields(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "location", required = false) String location,
            @RequestParam(value = "empName", required = false) String empName,
            @RequestParam(value = "desc", required = false) String desc,
            @RequestParam(value = "skills", required = false) List<Long> skills,
            @RequestParam("page") int page) {
        System.out.println("name: " + name);
        System.out.println("location: " + location);
        System.out.println("empName: " + empName);
        System.out.println("desc: " + desc);
        System.out.println("skills: " + skills);

        return jobOfferService.browseJobOffersByMultipleSeparateFields(name, location, empName, desc, skills, page);
    }


    @PostMapping("/advanced/{page}")
    public Page<JobOfferView> browseByMultipleSeparateFieldsPost(@RequestBody JobOfferView exampleJobOffer,
                                                         @PathVariable(name = "page") int page) {
        return jobOfferService.findJobOfferViewsByExample(exampleJobOffer, page);
    }

//    // Search po eden skill
//    @GetMapping("/skill/{skillId}/{page}")
//    public Page<JobOffer> browseJobOffersBySkill(@PathVariable(name = "skillId") Long skillId,
//                                                 @PathVariable(name = "page") int page) {
//        return jobOfferService.browseJobOffersBySkill(skillId, page);
//    }

    @GetMapping("/skills")
    public Page<JobOfferView> browseJobOffersBySkillsQueryString(@RequestParam(name = "skillIds") String skillIds,
                                                                 @RequestParam(name = "page") int page) {
        System.out.println(skillIds);
        System.out.println(page);
        String[] skillIdsStrings = skillIds.split(",");
        List<Long> skillIdsList = new ArrayList<>();
        for (String skillIdsString : skillIdsStrings) {
            if (skillIdsString.equals(""))
                continue;
            skillIdsList.add(Long.parseLong(skillIdsString));
        }
        //return jobOfferService.customFindAllByRequiredSkillsPage(skillIdsList, page);
        return jobOfferService.customFindAllByRequiredSkillsPage(skillIdsList, page);
    }

//    // PathVariable verzija
//    @GetMapping("/skills/{skillIds}/{page}")
//    public Page<JobOfferView> browseJobOffersBySkills(@PathVariable(name = "skillIds") List<Long> skillIds,
//                                                      //@PathVariable(name = "skillIds") String skillIds,
//                                                      @PathVariable(name = "page") int page) {
//        System.out.println(skillIds);
////        String[] skillIdsStrings = skillIds.split(",");
////        List<Long> skillIdsList = new ArrayList<>();
////        for (String skillIdsString : skillIdsStrings) {
////            if (skillIdsString.equals(""))
////                continue;
////            skillIdsList.add(Long.parseLong(skillIdsString));
////        }
//        //return jobOfferService.browseJobOffersBySkills(skillIdsList, page);
//        //return jobOfferService.customFindAllByRequiredSkills(skillIdsList);
//        return jobOfferService.customFindAllByRequiredSkillsPage(skillIds, page);
//    }

    // Logikata na metodov e: userot si gi gleda sopstvenite job offeri.
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @GetMapping("/employer/{employerId}/{page}")
    public Page<JobOfferView> viewJobOffersByUser(@PathVariable("employerId") Long employerId,
                                                  @PathVariable("page") int page) {
        //todo: Forbid access to all users except the 'employerId', or not.
        return jobOfferService.viewJobOffersByUser(employerId, page);
    }


    // prvo so ova ke zemes za edit, i otkoga ke klikne save ili whatever, posle e toa patch/put
    @GetMapping("/{jobId}/")
    public JobOffer getJobOfferById(@PathVariable(name = "jobId") Long jobOfferId) {
        Optional<JobOffer> jobOffer = jobOfferService.viewJobOffer(jobOfferId);
        if (jobOffer.isPresent())
            return jobOffer.get();
        else throw new NoSuchJobOfferException();
    }

}
