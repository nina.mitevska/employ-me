package com.jobs.service;

import com.jobs.config.security.UserPrincipal;
import com.jobs.model.JobOffer;
import com.jobs.model.views.JobOfferView;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

/**
 * Created by HTomovski
 * Date: 27-Oct-19
 * Time: 7:51 AM
 */
public interface JobOfferService {
    Page<JobOffer> browseJobOffersBySkill(Long skillId, int page);

    Page<JobOffer> browseJobOffersBySkills(List<Long> skillIds, int page);

    Optional<JobOffer> viewJobOffer(Long jobOfferId);

    JobOffer createJobOffer(JobOffer jobOffer, UserPrincipal userPrincipal);

    JobOffer editJobOffer(JobOffer jobOffer, UserPrincipal userPrincipal);

    void closeJobOffer(Long jobOfferId, UserPrincipal userPrincipal);

    Page<JobOfferView> findJobOfferViewsByExample(JobOfferView exampleJobOffer, int page);

    //Page<JobOffer> browseJobOffersByMultipleSeparateFieldsOld(JobOffer exampleJobOffer, int page);

    Page<JobOfferView> browseJobOffersByMultipleSeparateFields(
            String name,
            String location,
            String empName,
            String desc,
            List<Long> skillIds,
            int page);

    //Page<JobOffer> browseJobOffersByMultipleMixedFieldsOld(String propertiesString, int page);

    Page<JobOfferView> browseJobOffersByMultipleMixedFields(List<String> propertiesString, int page);


    List<JobOffer> customFindAllByRequiredSkills(List<Long> skillIds);

//    Page<JobOffer> customFindAllByRequiredSkillsPage(List<Long> skillIds, int page);
    Page<JobOfferView> customFindAllByRequiredSkillsPage(List<Long> skillIds, int page);

    Page<JobOfferView> viewJobOffersByUser(Long employerId, int page);

//    Page<JobSearchResult> customFindAllByRequiredSkillsPageJSR(List<Long> skillIds, int page);
}
