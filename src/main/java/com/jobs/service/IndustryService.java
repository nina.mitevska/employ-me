package com.jobs.service;

import com.jobs.model.Industry;

import java.util.List;

/**
 * Created by HTomovski
 * Date: 09-Sep-19
 * Time: 1:46 PM
 */
public interface IndustryService {
    List<Industry> getIndustries();

    Industry createIndustry(String industryName);
}
