package com.jobs.service.impl;

import com.jobs.model.Industry;
import com.jobs.persistence.IndustryJpaRepository;
import com.jobs.service.IndustryService;
import com.jobs.persistence.IndustryJpaRepository;
import com.jobs.service.IndustryService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HTomovski
 * Date: 09-Sep-19
 * Time: 1:46 PM
 */
@Service
public class IndustryServiceImpl implements IndustryService {
    private final IndustryJpaRepository industryRepository;

    public IndustryServiceImpl(IndustryJpaRepository industryRepository) {
        this.industryRepository = industryRepository;
    }

    @Override
    public List<Industry> getIndustries() {
        return industryRepository.findAll();
    }

    @Override
    public Industry createIndustry(String industryName) {
        return industryRepository.save(new Industry(industryName));
    }
}
