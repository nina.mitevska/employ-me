package com.jobs.service.impl;

import com.jobs.config.security.UserPrincipal;
import com.jobs.model.*;
import com.jobs.model.exceptions.EmailAlreadyTakenException;
import com.jobs.model.exceptions.NoSuchSkillException;
import com.jobs.model.exceptions.UserNotFoundException;
import com.jobs.persistence.*;
import com.jobs.service.UserService;
import com.jobs.model.Skill;
import com.jobs.persistence.JobOfferJpaRepository;
import com.jobs.persistence.SkillJpaRepository;
import com.jobs.persistence.UserJpaRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


/**
 * Created by HTomovski
 * Date: 22-Aug-19
 * Time: 3:22 PM
 */
@Service
public class UserServiceImpl implements UserService {
    private final UserJpaRepository userJpaRepository;
    private final SkillJpaRepository skillJpaRepository;
    private final JobOfferJpaRepository jobOfferJpaRepository;
    private final PasswordEncoder passwordEncoder;
    private final CandidateJpaRepository candidateJpaRepository;
    private final EmployerJpaRepository employerJpaRepository;

    @Value("${page-size}")
    private int PAGE_SIZE;

    public UserServiceImpl(UserJpaRepository userJpaRepository, SkillJpaRepository skillJpaRepository, JobOfferJpaRepository jobOfferJpaRepository, PasswordEncoder passwordEncoder, CandidateJpaRepository candidateJpaRepository, EmployerJpaRepository employerJpaRepository) {
        this.userJpaRepository = userJpaRepository;
        this.skillJpaRepository = skillJpaRepository;
        this.jobOfferJpaRepository = jobOfferJpaRepository;
        this.passwordEncoder = passwordEncoder;
        this.candidateJpaRepository = candidateJpaRepository;
        this.employerJpaRepository = employerJpaRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userJpaRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("User not found with email"));
        return UserPrincipal.create(user);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userJpaRepository.findByEmail(email);
    }

    @Override
    public Page<User> findAll(UserPrincipal currentLoggedInUser, Pageable pageable, String name, String email, String branchName) {
        return null;
    }

    @Override
    public UserDetails findUserById(Long id) {
        User databaseUser = userJpaRepository.findById(id).orElseThrow(UserNotFoundException::new);
        GrantedAuthority authorization = new SimpleGrantedAuthority(databaseUser.role.toString());
        return new UserPrincipal(databaseUser.id, databaseUser.name, databaseUser.email, databaseUser.password, "SYSTEM", "", Collections.singleton(authorization));
    }

    @Override
    public Optional<User> findById(Long userId) {
        return userJpaRepository.findById(userId);
    }

    @Override
    public Optional<Candidate> findCandidateById(Long userId) {
        return candidateJpaRepository.findById(userId);
    }

    @Override
    public Optional<Employer> findEmployerById(Long userId) {
        return employerJpaRepository.findById(userId);
    }

    @Override
    public User save(String name, String email, String password) {
        // Validate user email
        User testUser = userJpaRepository.findByEmail(email).orElse(null);
        if (testUser != null) {
            throw new EmailAlreadyTakenException();
        }

        // Otherwise create the user
        User user = User.createNewUser(name, email, "{bcrypt}" + passwordEncoder.encode(password));
        return userJpaRepository.save(user);
    }

    @Override
    public Candidate saveCandidate(String name, String email, String password) {
        // Validate user email
        User testUser = userJpaRepository.findByEmail(email).orElse(null);
        if (testUser != null) {
            throw new EmailAlreadyTakenException();
        }

        // Otherwise create the user
        User user = User.createNewUser(name, email, "{bcrypt}" + passwordEncoder.encode(password));
        Candidate candidate = new Candidate(user);
        return candidateJpaRepository.save(candidate);
    }

    @Override
    public Employer saveEmployer(String name, String email, String password) {
        // Validate user email
        User testUser = userJpaRepository.findByEmail(email).orElse(null);
        if (testUser != null) {
            throw new EmailAlreadyTakenException();
        }

        // Otherwise create the user
        User user = User.createNewUser(name, email, "{bcrypt}" + passwordEncoder.encode(password));
        Employer employer = new Employer(user);
        return userJpaRepository.save(employer);
    }

    @Override
    public User save(User user) {
        // Check if the email already exists
        User testUser = userJpaRepository.findByEmail(user.email).orElse(null);
        if (testUser != null) {
            user.password = testUser.password;
            user.id = testUser.id;
            user.role = testUser.role;
            return userJpaRepository.save(user);
        }

        // Otherwise create the user
        User.createNewUser(user.name, user.email, "{bcrypt}" + passwordEncoder.encode(user.password));
        return userJpaRepository.save(user);
    }

    @Override
    public Candidate updateCandidate(Candidate candidate) {
        User testUser = userJpaRepository.findByEmail(candidate.email).orElse(null);
        if (testUser != null) {
            candidate.password = testUser.password;
            candidate.id = testUser.id;
            candidate.role = testUser.role;
            return candidateJpaRepository.save(candidate);
        }
        return null;
    }

    @Override
    public Employer updateEmployer(Employer employer) {
        User testUser = userJpaRepository.findByEmail(employer.email).orElse(null);
        if (testUser != null) {
            employer.password = testUser.password;
            employer.id = testUser.id;
            employer.role = testUser.role;
            return employerJpaRepository.save(employer);
        }
        return null;
    }

    @Override
    public Page<Candidate> findAllBySkill(Long skillId, int pageNum) {
        Skill skill = null;
        Optional<Skill> skillOptional = skillJpaRepository.findById(skillId);
        if (skillOptional.isPresent()) {
            skill = skillOptional.get();
            return candidateJpaRepository.findBySkills(skill, PageRequest.of(pageNum, PAGE_SIZE));
        }
        throw new NoSuchSkillException();
    }

    @Override
    public List<Candidate> findAllBySkills(List<Long> skillIds) {
        Candidate exampleCandidate = new Candidate();
        exampleCandidate.skills = new ArrayList<>();
        for (Long skillId : skillIds) {
            Optional<Skill> skillOpt = skillJpaRepository.findById(skillId);
            skillOpt.ifPresent(exampleCandidate.skills::add);
        }


        Example<Candidate> userExample = Example.of(exampleCandidate);
        return candidateJpaRepository.findAll(userExample);
    }


}
