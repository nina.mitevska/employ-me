package com.jobs.service.impl;

import com.jobs.config.security.UserPrincipal;
import com.jobs.model.Employer;
import com.jobs.model.JobOffer;
import com.jobs.model.Ranking;
import com.jobs.model.Skill;
import com.jobs.model.exceptions.NoSuchEmployerException;
import com.jobs.model.exceptions.NoSuchSkillException;
import com.jobs.model.exceptions.NoSuchUserException;
import com.jobs.model.utils.AdvancedJobSearchParams;
import com.jobs.model.views.JobOfferView;
import com.jobs.persistence.*;
import com.jobs.service.JobOfferService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by HTomovski
 * Date: 27-Oct-19
 * Time: 7:55 AM
 */
@Service
public class JobOfferServiceImpl implements JobOfferService {

    @Value("${page-size}")
    private int PAGE_SIZE;
    private boolean flagToBeNamed = false;
    //private List<JobOffer> jobOfferFullQueryListOld = new ArrayList<>();
    private List<JobOfferView> jobOfferFullQueryList = new ArrayList<>();
    private List<String> currentQueryParams = new ArrayList<>();
    private final JobOfferJpaRepository jobOfferJpaRepository;
    private final JobOfferViewJpaRepository jobOfferViewJpaRepository;
    private final SkillJpaRepository skillJpaRepository;
    private final UserViewJpaRepository userViewJpaRepository;
    private final UserJpaRepository userJpaRepository;
    private final EmployerJpaRepository employerJpaRepository;
    private final CandidateJpaRepository candidateJpaRepository;
    private AdvancedJobSearchParams currentAdvParams = null;

    public JobOfferServiceImpl(JobOfferJpaRepository jobOfferJpaRepository, JobOfferViewJpaRepository jobOfferViewJpaRepository, SkillJpaRepository skillJpaRepository, UserViewJpaRepository userViewJpaRepository, UserJpaRepository userJpaRepository, EmployerJpaRepository employerJpaRepository, CandidateJpaRepository candidateJpaRepository) {
        this.jobOfferJpaRepository = jobOfferJpaRepository;
        this.jobOfferViewJpaRepository = jobOfferViewJpaRepository;
        this.skillJpaRepository = skillJpaRepository;
        this.userViewJpaRepository = userViewJpaRepository;
        this.userJpaRepository = userJpaRepository;
        this.employerJpaRepository = employerJpaRepository;
        this.candidateJpaRepository = candidateJpaRepository;
    }

    @Override
    public Page<JobOffer> browseJobOffersBySkill(Long skillId, int page) {
        Optional<Skill> skillOptional = skillJpaRepository.findById(skillId);
        if (skillOptional.isPresent()) {
            Skill skill = skillOptional.get();
            return jobOfferJpaRepository.findByRequiredSkills(skill, PageRequest.of(page, PAGE_SIZE));
        }
        throw new NoSuchSkillException();

    }

    @Override
    public Page<JobOffer> browseJobOffersBySkills(List<Long> skillIds, int page) {
        List<Skill> skills = new ArrayList<>();
        for (Long skillId : skillIds) {
            Optional<Skill> skillOpt = skillJpaRepository.findById(skillId);
            skillOpt.ifPresent(skills::add);
        }
        System.out.println(skillIds.size());
        return jobOfferJpaRepository.findAllByRequiredSkills(skills, PageRequest.of(page, PAGE_SIZE));
    }

    @Override
    public Optional<JobOffer> viewJobOffer(Long jobOfferId) {
        return jobOfferJpaRepository.findById(jobOfferId);
    }

    @Override
    public JobOffer createJobOffer(JobOffer jobOffer, UserPrincipal userPrincipal) {
        System.out.println(jobOffer.employer.id);
        System.out.println(userPrincipal.getId());
        if (jobOffer.employer.id.equals(userPrincipal.getId())) {
            employerJpaRepository.findById(jobOffer.employer.id).ifPresent(emp -> jobOffer.employer = emp);
            if (jobOffer.employer == null)
                throw new NoSuchEmployerException();
//            for (Skill requiredSkill : jobOffer.requiredSkills) {
//                jobOffer.requiredSkills.remove(requiredSkill);
//                Optional<Skill> existingSkillOpt = skillJpaRepository.findById(requiredSkill.getId());
//                existingSkillOpt.ifPresent(skill -> jobOffer.requiredSkills.add(skill));
//            }
            return jobOfferJpaRepository.save(jobOffer);
        }

        throw new NoSuchEmployerException();
    }

    @Override
    public JobOffer editJobOffer(JobOffer jobOffer, UserPrincipal userPrincipal) {
        Optional<JobOffer> existingJobOpt = jobOfferJpaRepository.findById(jobOffer.id);
//        System.out.println("Existing");
//        System.out.println(existing.orElse(null));
//        System.out.println("New");
//        System.out.println(jobOffer);
        if (jobOffer.employer.id.equals(userPrincipal.getId()))
            return jobOfferJpaRepository.save(jobOffer);
        else {
            System.out.println("=== JOB CREATOR AND CURRENT USER ID MISMATCH IN EDIT SAVE ===");
            return null;
        }
    }

    @Override
    public void closeJobOffer(Long jobOfferId,
                              UserPrincipal userPrincipal) {
//        JobOffer jobOffer = jobOfferJpaRepository.findById(jobOfferId).orElse(null);
        JobOfferView jobOfferView = jobOfferViewJpaRepository.getById(jobOfferId);
//        System.out.println(jobOffer);
//        System.out.println(jobOffer.employer);
//        System.out.println(jobOfferView);
//        System.out.println(jobOfferView.employer);
        if (jobOfferView.employer.id.equals(userPrincipal.getId()))
            jobOfferJpaRepository.deleteById(jobOfferId);
        else {
            System.out.println("=== JOB CREATOR AND CURRENT USER ID MISMATCH IN JOB DELETE ===");
        }
    }

    @Override
    public Page<JobOfferView> findJobOfferViewsByExample(JobOfferView exampleJobOffer, int page) {
        ExampleMatcher exampleMatcher = ExampleMatcher
                .matchingAll()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        return jobOfferViewJpaRepository.findAll(Example.of(exampleJobOffer, exampleMatcher), PageRequest.of(page, PAGE_SIZE));
    }


    @Override
    public Page<JobOfferView> browseJobOffersByMultipleSeparateFields(
            String name,
            String location,
            String empName,
            String desc,
            List<Long> skillIds,
            int page) {

        JobOfferView jobOfferView = new JobOfferView();
        jobOfferView.name = name;
        jobOfferView.location = location;
        if (empName != null)
            jobOfferView.employer = this.userViewJpaRepository.findByNameContaining(empName);
        jobOfferView.shortJobDescription = desc;
        if (skillIds != null && skillIds.size() > 0)  {
            jobOfferView.requiredSkills = new ArrayList<Skill>();
            jobOfferView.requiredSkills.addAll(
                    skillIds.stream()
                            .map(skillId -> this.skillJpaRepository.findById(skillId).orElse(null))
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList()));
        }

        System.out.println("exampleJob.employer: ");
        System.out.println(jobOfferView.employer);
        System.out.println("exampleJob.skills: ");
        System.out.println(jobOfferView.requiredSkills);
        ExampleMatcher exampleMatcher = ExampleMatcher
                .matchingAll()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

        Example<JobOfferView> example = Example.of(jobOfferView, exampleMatcher);
        //return this.jobOfferViewJpaRepository.findAll(example, PageRequest.of(page, PAGE_SIZE));

        // komplikacii nepotrebni
        List<JobOfferView> jobs = this.jobOfferViewJpaRepository.findAll(example);


        //ToDo: possibly figure out a better way to add the skills to the query
        // (query by example with a list attribute)
        if (jobOfferView.requiredSkills != null) {

            jobs.removeIf(job -> !job.requiredSkills.containsAll(jobOfferView.requiredSkills));
//            for (int i = 0; i < jobs.size(); i++) {
//                if (!jobs.get(i).requiredSkills.containsAll(jobOfferView.requiredSkills)) {
//                    jobs.remove(i);
//                    i--;
//                }
//            }
        }


        int startIndex = page * PAGE_SIZE, endIndex = (page + 1) * PAGE_SIZE;
        if (startIndex > jobs.size())
            return null; // ne bi trebalo da se sluci, treba uste na front end da se sredi
        if (endIndex > jobs.size()) {
            endIndex = jobs.size();
        }
        Page<JobOfferView> pageToReturn = new PageImpl<JobOfferView>(jobs.subList(startIndex, endIndex), PageRequest.of(page, PAGE_SIZE), jobs.size());

        System.out.println(pageToReturn);
        return pageToReturn;
    }
/*
        // declaration of the list I will keep the jobs in
        List<JobOfferView> jobOffers = new ArrayList<>();

        // Searching by all fields present
        if (name != null)
            jobOffers.addAll(jobOfferViewJpaRepository.findAllByNameContaining(name));
        if (location != null)
            jobOffers.addAll(jobOfferViewJpaRepository.findAllByLocationContaining(location));
        if (desc != null)
            jobOffers.addAll(jobOfferViewJpaRepository.findAllByShortJobDescriptionContaining(desc));
            List<User> employers = new ArrayList<>();
        if (empName != null) {
            employers.addAll(userJpaRepository.findAllByNameContaining(empName));
            for (User employer : employers) {
                jobOffers.addAll(jobOfferViewJpaRepository.getJobOffersByUserIdList(employer.id));
            }
        }
        if (skillIds != null && skillIds.size() > 0) {
            jobOffers.addAll(jobOfferViewJpaRepository.customFindAllByRequiredSkillsList(skillIds, skillIds.size()));
        }

        // Removing duplicate jobs
        jobOffers = jobOffers.stream().distinct().collect(Collectors.toList());
        jobOffers.forEach(System.out::println);

        // rank the resulting job offers

        List<Ranking<JobOfferView>> rankings = jobOffers.stream()
                .map(job -> new Ranking<JobOfferView>(job, 0))
                .collect(Collectors.toList());
        // description da dava najmalku poeni, name i location poveke?

        if (name != null)
            rankings.stream()
                    .filter(r -> r.getItem().getName().toLowerCase().contains(name.toLowerCase()))
                    .forEach(r -> r.rankPoints += 5);
        if (location != null)
            rankings.stream()
                    .filter(r -> r.getItem().getLocation().toLowerCase().contains(location.toLowerCase()))
                    .forEach(r -> r.rankPoints += 5);
        if (desc != null)
            rankings.stream()
                    .filter(r -> r.getItem().getShortJobDescription().toLowerCase().contains(desc.toLowerCase()))
                    .forEach(r -> r.rankPoints += 2);

        // Logikata na ova e: ako userot searchnal po employer name, i imalo takvi employeri
        if (empName != null && employers.size() > 0) {
            // na job offerite koi se od nekoj searchnat employer, daj im +3 poeni
            // (se proveruva po id, a eden job ima samo eden employer, taka da ne moze da se dobie dva pati +3)
            for (final User employer : employers) {
                rankings.stream()
                        .filter(r -> r.getItem().getEmployer().getId().equals(employer.getId()))
                        .forEach(r -> r.rankPoints += 3);
            }

        }
        // Ako bil selektiran barem eden skill
        if (skillIds != null && skillIds.size() > 0) {

            // Za sekoj job
            for (Ranking<JobOfferView> ranking : rankings) {
                // Za sekoj skill vnesen vo searchot
                for (Long searchSkillId : skillIds) {
                    // za sekoj skill od searchot sto se poklopuva so skillovite na job, +3
                    for (Skill rankingSkillReq : ranking.getItem().getRequiredSkills()) {
                        if (searchSkillId.equals(rankingSkillReq.getId()))
                            ranking.rankPoints += 3;
                    }

                }
            }
        }

        // return page by page

        System.out.println("======== Sorted ========");
        rankings.forEach(System.out::println);
        System.out.println(page);
        jobOffers = rankings.stream().map(r -> r.item).collect(Collectors.toList());
        //this.jobOfferFullQueryList = jobOffers;
        //System.out.println(jobOffers.subList(page*PAGE_SIZE, (page+1)*PAGE_SIZE));
        int startIndex = page * PAGE_SIZE, endIndex = (page + 1) * PAGE_SIZE;
        if (startIndex > jobOffers.size())
            return null; // ne bi trebalo da se desi, treba uste na front end da se sredi
        if (endIndex > jobOffers.size()) {
            endIndex = jobOffers.size();
        }
        System.out.println("startIndex = " + startIndex + ", endIndex = " + endIndex);
        Page<JobOfferView> pageToReturn = new PageImpl<JobOfferView>(jobOffers.subList(startIndex, endIndex), PageRequest.of(page, PAGE_SIZE), jobOffers.size());
        System.out.println("Page:" + pageToReturn.getContent());
        return pageToReturn;
    }
 */



    @Override
    public Page<JobOfferView> browseJobOffersByMultipleMixedFields(List<String> propertiesList, int page) {

        List<JobOfferView> jobOffers = new ArrayList<>();


        // Querying the database for each parameter, and removing duplicates
        for (String property : propertiesList) {
            jobOffers.addAll(jobOfferViewJpaRepository.customFindAllBySingleUnknownField(property));
        }
        jobOffers = jobOffers.stream().distinct().collect(Collectors.toList());
        jobOffers.forEach(System.out::println);

        // Ranking the results by relevance
        List<Ranking<JobOfferView>> rankings = jobOffers.stream()
                .map(job -> new Ranking<JobOfferView>(job, 0))
                .collect(Collectors.toList());

        for (Ranking<JobOfferView> ranking : rankings) {
            //UserViewShort employer = userViewJpaRepository.getById(ranking.item.employer.getId());
            for (String property : propertiesList) {
                if (ranking.item.name.toLowerCase().contains(property.toLowerCase()))
                    ranking.rankPoints += 5;
                if (ranking.item.location.toLowerCase().contains(property.toLowerCase()))
                    ranking.rankPoints += 5;
                if (ranking.item.employer.getName().toLowerCase().contains(property.toLowerCase()))
                    ranking.rankPoints += 3;
                if (ranking.item.shortJobDescription.toLowerCase().contains(property.toLowerCase()))
                    ranking.rankPoints += 2;
                for (Skill skill : ranking.item.requiredSkills) {
                    if (skill.getName().toLowerCase().contains(property.toLowerCase()))
                        ranking.rankPoints += 3;
                }

                if (ranking.item.requiredSkills.get(0)
                        .getIndustry()
                        .toString()
                        .toLowerCase()
                        .contains(property.toLowerCase()))
                    ranking.rankPoints += 1;
            }
            System.out.println(ranking);
        }
        rankings.sort(Comparator.reverseOrder());

        System.out.println("======== Sorted ========");
        rankings.forEach(System.out::println);
        System.out.println(page);
        jobOffers = rankings.stream().map(r -> r.item).collect(Collectors.toList());
        //System.out.println(jobOffers.subList(page*PAGE_SIZE, (page+1)*PAGE_SIZE));
        int startIndex = page * PAGE_SIZE, endIndex = (page + 1) * PAGE_SIZE;
        if (startIndex > jobOffers.size())
            return null; // ne bi trebalo da se sluci, treba uste na front end da se sredi
        if (endIndex > jobOffers.size()) {
            endIndex = jobOffers.size();
        }
        System.out.println("startIndex = " + startIndex + ", endIndex = " + endIndex);
        Page<JobOfferView> pageToReturn = new PageImpl<JobOfferView>(jobOffers.subList(startIndex, endIndex), PageRequest.of(page, PAGE_SIZE), jobOffers.size());
        System.out.println("Page:" + pageToReturn.getContent());
        return pageToReturn;
        //return jobOfferJpaRepository.customFindAllByMultipleMixedFields(propertiesArray, PageRequest.of(page, PAGE_SIZE));
    }


    @Override
    public List<JobOffer> customFindAllByRequiredSkills(List<Long> skillIds) {
        return jobOfferJpaRepository.customFindAllByRequiredSkills(skillIds, skillIds.size());
    }


//    @Override
//    public Page<JobOffer> customFindAllByRequiredSkillsPage(List<Long> skillIds, int page) {
//        return jobOfferJpaRepository.customFindAllByRequiredSkillsPage(skillIds, skillIds.size(), PageRequest.of(page, PAGE_SIZE));
//    }

    @Override
    public Page<JobOfferView> customFindAllByRequiredSkillsPage(List<Long> skillIds, int page) {
        return jobOfferViewJpaRepository.customFindAllByRequiredSkills(skillIds, skillIds.size(), PageRequest.of(page, PAGE_SIZE));
    }

    @Override
    public Page<JobOfferView> viewJobOffersByUser(Long employerId, int page) {
        return jobOfferViewJpaRepository.getJobOffersByUserId(employerId, PageRequest.of(page, PAGE_SIZE));
    }

//    @Override
//    public Page<JobSearchResult> customFindAllByRequiredSkillsPageJSR(List<Long> skillIds, int page) {
//        return jobOfferJpaRepository.customFindAllByRequiredSkillsPageJSR(skillIds, skillIds.size(), PageRequest.of(page, PAGE_SIZE));
//    }
}
