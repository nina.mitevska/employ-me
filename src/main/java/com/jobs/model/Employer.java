package com.jobs.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "employer")
public class Employer extends User {
//    @OneToMany(targetEntity = JobOffer.class, fetch = FetchType.LAZY)
//    public List<JobOffer> jobOffers;
    public String companyInfo;
    public Employer() {
    }

    public Employer(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.password = user.getPassword();
        this.role = user.getRole();
    }


}
