package com.jobs.model.views;

import com.jobs.model.JobOffer;
import com.jobs.model.Skill;
import com.jobs.model.User;
import com.jobs.model.Skill;
import org.springframework.data.annotation.Immutable;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Immutable
@Table(name = "job_offer")
public class JobOfferView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public String name;

    public String location;

    //@Column(name = "emp_id")
    // od konsultaciite - preporakata bese da se koristi samo id za employerot, ama jas si napraviv view so id i name.
    @ManyToOne
    @JoinColumn(name = "emp_id")
    public UserViewShort employer;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "job_required_skills",
            joinColumns = @JoinColumn(name = "job_offer_id"),
            inverseJoinColumns = @JoinColumn(name = "required_skill_id"))
    public List<Skill> requiredSkills;

    @Column(name = "short_desc")
    public String shortJobDescription;

//    public static JobOfferView from(String[] args) {
//        JobOfferView jobOfferView;
//
//        return
//    }

    public JobOfferView() {
    }

    public JobOfferView(String name,
                        String location,
                        UserViewShort employer,
                        List<Skill> requiredSkills,
                        String shortJobDescription) {
        this.name = name;
        this.location = location;
        this.employer = employer;
        this.requiredSkills = requiredSkills;
        this.shortJobDescription = shortJobDescription;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public UserViewShort getEmployer() {
        return employer;
    }

    public void setEmployer(UserViewShort employerId) {
        this.employer = employerId;
    }

    public List<Skill> getRequiredSkills() {
        return requiredSkills;
    }

    public void setRequiredSkills(List<Skill> requiredSkills) {
        this.requiredSkills = requiredSkills;
    }

    public String getShortJobDescription() {
        return shortJobDescription;
    }

    public void setShortJobDescription(String shortJobDescription) {
        this.shortJobDescription = shortJobDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobOfferView jobOffer = (JobOfferView) o;
        return Objects.equals(id, jobOffer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "JobOffer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", employer=" + employer +
                ", requiredSkills=" + requiredSkills +
                ", shortJobDescription='" + shortJobDescription + '\'' +
                '}';
    }
}
