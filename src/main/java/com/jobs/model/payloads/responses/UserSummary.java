package com.jobs.model.payloads.responses;

import com.jobs.model.Role;

public class UserSummary {
    public Long id;
    public String name;
    public String email;
    public String role;
}
