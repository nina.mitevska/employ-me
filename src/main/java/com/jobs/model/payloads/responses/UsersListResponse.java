package com.jobs.model.payloads.responses;

import com.jobs.model.User;
import com.jobs.model.User;

import java.util.List;

public class UsersListResponse {
    public Long totalElements;
    public Integer totalPages;
    public List<User> users;

    public UsersListResponse(Long totalElements, Integer totalPages, List<User> users) {
        this.totalElements = totalElements;
        this.totalPages = totalPages;
        this.users = users;
    }
}
