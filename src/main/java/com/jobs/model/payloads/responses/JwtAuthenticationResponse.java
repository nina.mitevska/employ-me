package com.jobs.model.payloads.responses;


import org.springframework.beans.factory.annotation.Value;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;


public class JwtAuthenticationResponse {
//    @Value("${app.jwtExpirationInMs}")
//    private int jwtExpirationInMs;
    private String accessToken;
    private String tokenType = "Bearer";
    private long expiresAt;

    public JwtAuthenticationResponse(String accessToken, int jwtExpirationInMs) {
        this.accessToken = accessToken;
//        System.out.println("jwtExpInMs: " + jwtExpirationInMs);
//        System.out.println(Instant.now().toEpochMilli());
//        System.out.println(Instant.now().toEpochMilli() + jwtExpirationInMs);
        this.expiresAt = Instant.now().plus(Duration.of(jwtExpirationInMs, ChronoUnit.MILLIS)).toEpochMilli();
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public long getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(long expiresAt) {
        this.expiresAt = expiresAt;
    }
}
