package com.jobs.model.payloads.requests;

import javax.validation.constraints.NotNull;

public class ChangePasswordRequest {
    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @NotNull
    private String currentPassword;

    @NotNull
    private String newPassword;
}
