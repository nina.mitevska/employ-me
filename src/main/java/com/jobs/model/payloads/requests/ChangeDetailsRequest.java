package com.jobs.model.payloads.requests;

import javax.validation.constraints.NotNull;

public class ChangeDetailsRequest {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    private String name;
}
