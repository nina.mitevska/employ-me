package com.jobs.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "candidate")
public class Candidate extends User {

    public String CV;

    @Column(name = "past_employments")
    public String pastEmployments;
    @ManyToMany
    @JoinTable(name = "candidate_skill",
            joinColumns = @JoinColumn(name = "candidate_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id"))
    public List<Skill> skills;

    public Candidate() {
    }

    public Candidate(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.password = user.getPassword();
        this.role = user.getRole();
    }


    public String getCV() {
        return CV;
    }

    public void setCV(String CV) {
        this.CV = CV;
    }

    public String getPastEmployments() {
        return pastEmployments;
    }

    public void setPastEmployments(String pastEmployments) {
        this.pastEmployments = pastEmployments;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", CV='" + CV + '\'' +
                ", pastEmployments='" + pastEmployments + '\'' +
                ", skills=" + skills +
                '}';
    }
}
